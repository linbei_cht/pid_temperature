/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "widgets_init.h"
#include "custom.h"


void setup_scr_screen_main(lv_ui *ui)
{
	//Write codes screen_main
	ui->screen_main = lv_obj_create(NULL);
	ui->g_kb_screen_main = lv_keyboard_create(ui->screen_main);
	lv_obj_add_event_cb(ui->g_kb_screen_main, kb_event_cb, LV_EVENT_ALL, NULL);
	lv_obj_add_flag(ui->g_kb_screen_main, LV_OBJ_FLAG_HIDDEN);
	lv_obj_set_style_text_font(ui->g_kb_screen_main, &lv_font_simhei_18, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_size(ui->screen_main, 480, 320);

	//Write style for screen_main, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main, lv_color_hex(0x1f1e1e), LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_tabview_1
	ui->screen_main_tabview_1 = lv_tabview_create(ui->screen_main, LV_DIR_RIGHT, 70);
	lv_obj_set_pos(ui->screen_main_tabview_1, 0, 0);
	lv_obj_set_size(ui->screen_main_tabview_1, 480, 320);
	lv_obj_set_scrollbar_mode(ui->screen_main_tabview_1, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_tabview_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_tabview_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_tabview_1, lv_color_hex(0x1f1e1e), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_tabview_1, lv_color_hex(0x0beb9f), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_tabview_1, &lv_font_simhei_30, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_tabview_1, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_tabview_1, 16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_tabview_1, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_tabview_1, 100, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_tabview_1, lv_color_hex(0xc0c0c0), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_tabview_1, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_tabview_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_tabview_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_DEFAULT for &style_screen_main_tabview_1_extra_btnm_main_default
	static lv_style_t style_screen_main_tabview_1_extra_btnm_main_default;
	ui_init_style(&style_screen_main_tabview_1_extra_btnm_main_default);
	
	lv_style_set_bg_opa(&style_screen_main_tabview_1_extra_btnm_main_default, 255);
	lv_style_set_bg_color(&style_screen_main_tabview_1_extra_btnm_main_default, lv_color_hex(0x000000));
	lv_style_set_border_width(&style_screen_main_tabview_1_extra_btnm_main_default, 0);
	lv_style_set_radius(&style_screen_main_tabview_1_extra_btnm_main_default, 0);
	lv_obj_add_style(lv_tabview_get_tab_btns(ui->screen_main_tabview_1), &style_screen_main_tabview_1_extra_btnm_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_DEFAULT for &style_screen_main_tabview_1_extra_btnm_items_default
	static lv_style_t style_screen_main_tabview_1_extra_btnm_items_default;
	ui_init_style(&style_screen_main_tabview_1_extra_btnm_items_default);
	
	lv_style_set_text_color(&style_screen_main_tabview_1_extra_btnm_items_default, lv_color_hex(0xff6d0d));
	lv_style_set_text_font(&style_screen_main_tabview_1_extra_btnm_items_default, &lv_font_simhei_20);
	lv_obj_add_style(lv_tabview_get_tab_btns(ui->screen_main_tabview_1), &style_screen_main_tabview_1_extra_btnm_items_default, LV_PART_ITEMS|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_CHECKED for &style_screen_main_tabview_1_extra_btnm_items_checked
	static lv_style_t style_screen_main_tabview_1_extra_btnm_items_checked;
	ui_init_style(&style_screen_main_tabview_1_extra_btnm_items_checked);
	
	lv_style_set_text_color(&style_screen_main_tabview_1_extra_btnm_items_checked, lv_color_hex(0x2195f6));
	lv_style_set_text_font(&style_screen_main_tabview_1_extra_btnm_items_checked, &lv_font_simsun_12);
	lv_style_set_border_width(&style_screen_main_tabview_1_extra_btnm_items_checked, 4);
	lv_style_set_border_opa(&style_screen_main_tabview_1_extra_btnm_items_checked, 255);
	lv_style_set_border_color(&style_screen_main_tabview_1_extra_btnm_items_checked, lv_color_hex(0x2195f6));
	lv_style_set_border_side(&style_screen_main_tabview_1_extra_btnm_items_checked, LV_BORDER_SIDE_BOTTOM);
	lv_style_set_radius(&style_screen_main_tabview_1_extra_btnm_items_checked, 0);
	lv_style_set_bg_opa(&style_screen_main_tabview_1_extra_btnm_items_checked, 60);
	lv_style_set_bg_color(&style_screen_main_tabview_1_extra_btnm_items_checked, lv_color_hex(0x2195f6));
	lv_obj_add_style(lv_tabview_get_tab_btns(ui->screen_main_tabview_1), &style_screen_main_tabview_1_extra_btnm_items_checked, LV_PART_ITEMS|LV_STATE_CHECKED);

	//Write codes 主页
	ui->screen_main_tabview_1_tab_1 = lv_tabview_add_tab(ui->screen_main_tabview_1,"主页");
	lv_obj_t * screen_main_tabview_1_tab_1_label = lv_label_create(ui->screen_main_tabview_1_tab_1);
	lv_label_set_text(screen_main_tabview_1_tab_1_label, "");

	//Write codes screen_main_cont_1
	ui->screen_main_cont_1 = lv_obj_create(ui->screen_main_tabview_1_tab_1);
	lv_obj_set_pos(ui->screen_main_cont_1, 10, 13);
	lv_obj_set_size(ui->screen_main_cont_1, 361, 100);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_1, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_1, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_cont_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_cont_1, lv_color_hex(0x444444), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_cont_1, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_1
	ui->screen_main_label_1 = lv_label_create(ui->screen_main_cont_1);
	lv_label_set_text(ui->screen_main_label_1, "PV");
	lv_label_set_long_mode(ui->screen_main_label_1, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_1, 18, 28);
	lv_obj_set_size(ui->screen_main_label_1, 75, 50);

	//Write style for screen_main_label_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_1, lv_color_hex(0x0aff12), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_1, &lv_font_simhei_30, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_1, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_1, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_pv_value
	ui->screen_main_label_pv_value = lv_label_create(ui->screen_main_cont_1);
	lv_label_set_text(ui->screen_main_label_pv_value, "0");
	lv_label_set_long_mode(ui->screen_main_label_pv_value, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_pv_value, 132, 25);
	lv_obj_set_size(ui->screen_main_label_pv_value, 201, 50);

	//Write style for screen_main_label_pv_value, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_pv_value, lv_color_hex(0x0aff12), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_pv_value, &lv_font_montserratMedium_35, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_pv_value, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_pv_value, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_pv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_cont_2
	ui->screen_main_cont_2 = lv_obj_create(ui->screen_main_tabview_1_tab_1);
	lv_obj_set_pos(ui->screen_main_cont_2, 10, 126);
	lv_obj_set_size(ui->screen_main_cont_2, 363, 100);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_2, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_2, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_cont_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_cont_2, lv_color_hex(0x444444), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_cont_2, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_3
	ui->screen_main_label_3 = lv_label_create(ui->screen_main_cont_2);
	lv_label_set_text(ui->screen_main_label_3, "SV");
	lv_label_set_long_mode(ui->screen_main_label_3, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_3, 15, 25);
	lv_obj_set_size(ui->screen_main_label_3, 83, 50);

	//Write style for screen_main_label_3, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_3, lv_color_hex(0xf55656), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_3, &lv_font_simhei_30, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_3, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_sv_value
	ui->screen_main_label_sv_value = lv_label_create(ui->screen_main_cont_2);
	lv_label_set_text(ui->screen_main_label_sv_value, "0");
	lv_label_set_long_mode(ui->screen_main_label_sv_value, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_sv_value, 132, 25);
	lv_obj_set_size(ui->screen_main_label_sv_value, 204, 50);

	//Write style for screen_main_label_sv_value, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_sv_value, lv_color_hex(0xf55656), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_sv_value, &lv_font_montserratMedium_35, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_sv_value, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_sv_value, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_sv_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_led_out
	ui->screen_main_led_out = lv_led_create(ui->screen_main_tabview_1_tab_1);
	lv_led_set_brightness(ui->screen_main_led_out, 255);
	lv_led_set_color(ui->screen_main_led_out, lv_color_hex(0xff0034));
	lv_obj_set_pos(ui->screen_main_led_out, 321, 242);
	lv_obj_set_size(ui->screen_main_led_out, 28, 29);

	//Write codes 数据
	ui->screen_main_tabview_1_tab_2 = lv_tabview_add_tab(ui->screen_main_tabview_1,"数据");
	lv_obj_t * screen_main_tabview_1_tab_2_label = lv_label_create(ui->screen_main_tabview_1_tab_2);
	lv_label_set_text(screen_main_tabview_1_tab_2_label, "");

	//Write codes screen_main_chart_temperature
	ui->screen_main_chart_temperature = lv_chart_create(ui->screen_main_tabview_1_tab_2);
	lv_chart_set_type(ui->screen_main_chart_temperature, LV_CHART_TYPE_LINE);
	lv_chart_set_div_line_count(ui->screen_main_chart_temperature, 10, 40);
	lv_chart_set_point_count(ui->screen_main_chart_temperature, 5);
	lv_chart_set_range(ui->screen_main_chart_temperature, LV_CHART_AXIS_PRIMARY_Y, 0, 100);
	lv_chart_set_range(ui->screen_main_chart_temperature, LV_CHART_AXIS_SECONDARY_Y, 0, 100);
	lv_chart_set_zoom_x(ui->screen_main_chart_temperature, 256);
	lv_chart_set_zoom_y(ui->screen_main_chart_temperature, 256);
	lv_obj_set_pos(ui->screen_main_chart_temperature, 19, 18);
	lv_obj_set_size(ui->screen_main_chart_temperature, 356, 251);
	lv_obj_set_scrollbar_mode(ui->screen_main_chart_temperature, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_chart_temperature, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_chart_temperature, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_chart_temperature, lv_color_hex(0x1f1e1e), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_chart_temperature, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_chart_temperature, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_chart_temperature, lv_color_hex(0x1f1e1e), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_chart_temperature, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_chart_temperature, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_line_width(ui->screen_main_chart_temperature, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_line_color(ui->screen_main_chart_temperature, lv_color_hex(0x2c2626), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_line_opa(ui->screen_main_chart_temperature, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_chart_temperature, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_chart_temperature, Part: LV_PART_TICKS, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_chart_temperature, lv_color_hex(0x020202), LV_PART_TICKS|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_chart_temperature, &lv_font_montserratMedium_16, LV_PART_TICKS|LV_STATE_DEFAULT);
	lv_obj_set_style_line_width(ui->screen_main_chart_temperature, 2, LV_PART_TICKS|LV_STATE_DEFAULT);
	lv_obj_set_style_line_color(ui->screen_main_chart_temperature, lv_color_hex(0x0b2012), LV_PART_TICKS|LV_STATE_DEFAULT);
	lv_obj_set_style_line_opa(ui->screen_main_chart_temperature, 255, LV_PART_TICKS|LV_STATE_DEFAULT);

	//Write codes 设置
	ui->screen_main_tabview_1_tab_3 = lv_tabview_add_tab(ui->screen_main_tabview_1,"设置");
	lv_obj_t * screen_main_tabview_1_tab_3_label = lv_label_create(ui->screen_main_tabview_1_tab_3);
	lv_label_set_text(screen_main_tabview_1_tab_3_label, "");

	//Write codes screen_main_cont_9
	ui->screen_main_cont_9 = lv_obj_create(ui->screen_main_tabview_1_tab_3);
	lv_obj_set_pos(ui->screen_main_cont_9, 20, 18);
	lv_obj_set_size(ui->screen_main_cont_9, 343, 47);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_9, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_9, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_9, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_9, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_9, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_9, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_9, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_9, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_9, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_9, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_10
	ui->screen_main_label_10 = lv_label_create(ui->screen_main_cont_9);
	lv_label_set_text(ui->screen_main_label_10, "SV");
	lv_label_set_long_mode(ui->screen_main_label_10, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_10, 27, 15);
	lv_obj_set_size(ui->screen_main_label_10, 90, 17);

	//Write style for screen_main_label_10, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_10, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_10, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_10, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_10, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_txb_sv
	ui->screen_main_txb_sv = lv_textarea_create(ui->screen_main_cont_9);
	lv_textarea_set_text(ui->screen_main_txb_sv, "0");
	lv_textarea_set_password_bullet(ui->screen_main_txb_sv, "*");
	lv_textarea_set_password_mode(ui->screen_main_txb_sv, false);
	lv_textarea_set_one_line(ui->screen_main_txb_sv, false);
	#if LV_USE_KEYBOARD != 0 || LV_USE_ZH_KEYBOARD != 0
		lv_obj_add_event_cb(ui->screen_main_txb_sv, ta_event_cb, LV_EVENT_ALL, ui->g_kb_screen_main);
	#endif
	lv_obj_set_pos(ui->screen_main_txb_sv, 142, 6);
	lv_obj_set_size(ui->screen_main_txb_sv, 200, 35);

	//Write style for screen_main_txb_sv, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_txb_sv, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_txb_sv, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_txb_sv, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_txb_sv, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_txb_sv, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_sv, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_txb_sv, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_txb_sv, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_txb_sv, lv_color_hex(0xe6e6e6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_txb_sv, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_txb_sv, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_txb_sv, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_txb_sv, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_txb_sv, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_sv, 4, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_txb_sv, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_txb_sv, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_sv, lv_color_hex(0x2195f6), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_sv, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write codes screen_main_cont_10
	ui->screen_main_cont_10 = lv_obj_create(ui->screen_main_tabview_1_tab_3);
	lv_obj_set_pos(ui->screen_main_cont_10, 19, 77);
	lv_obj_set_size(ui->screen_main_cont_10, 346, 47);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_10, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_10, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_11
	ui->screen_main_label_11 = lv_label_create(ui->screen_main_cont_10);
	lv_label_set_text(ui->screen_main_label_11, "Kp");
	lv_label_set_long_mode(ui->screen_main_label_11, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_11, 27, 15);
	lv_obj_set_size(ui->screen_main_label_11, 90, 17);

	//Write style for screen_main_label_11, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_11, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_11, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_11, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_11, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_txb_kp
	ui->screen_main_txb_kp = lv_textarea_create(ui->screen_main_cont_10);
	lv_textarea_set_text(ui->screen_main_txb_kp, "0");
	lv_textarea_set_password_bullet(ui->screen_main_txb_kp, "*");
	lv_textarea_set_password_mode(ui->screen_main_txb_kp, false);
	lv_textarea_set_one_line(ui->screen_main_txb_kp, false);
	#if LV_USE_KEYBOARD != 0 || LV_USE_ZH_KEYBOARD != 0
		lv_obj_add_event_cb(ui->screen_main_txb_kp, ta_event_cb, LV_EVENT_ALL, ui->g_kb_screen_main);
	#endif
	lv_obj_set_pos(ui->screen_main_txb_kp, 142, 6);
	lv_obj_set_size(ui->screen_main_txb_kp, 200, 35);

	//Write style for screen_main_txb_kp, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_txb_kp, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_txb_kp, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_txb_kp, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_txb_kp, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_txb_kp, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_kp, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_txb_kp, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_txb_kp, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_txb_kp, lv_color_hex(0xe6e6e6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_txb_kp, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_txb_kp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_txb_kp, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_txb_kp, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_txb_kp, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_kp, 4, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_txb_kp, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_txb_kp, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_kp, lv_color_hex(0x2195f6), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_kp, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write codes screen_main_cont_11
	ui->screen_main_cont_11 = lv_obj_create(ui->screen_main_tabview_1_tab_3);
	lv_obj_set_pos(ui->screen_main_cont_11, 18, 136);
	lv_obj_set_size(ui->screen_main_cont_11, 345, 47);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_11, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_11, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_11, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_12
	ui->screen_main_label_12 = lv_label_create(ui->screen_main_cont_11);
	lv_label_set_text(ui->screen_main_label_12, "Ki");
	lv_label_set_long_mode(ui->screen_main_label_12, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_12, 27, 15);
	lv_obj_set_size(ui->screen_main_label_12, 90, 17);

	//Write style for screen_main_label_12, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_12, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_12, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_12, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_12, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_txb_ki
	ui->screen_main_txb_ki = lv_textarea_create(ui->screen_main_cont_11);
	lv_textarea_set_text(ui->screen_main_txb_ki, "0");
	lv_textarea_set_password_bullet(ui->screen_main_txb_ki, "*");
	lv_textarea_set_password_mode(ui->screen_main_txb_ki, false);
	lv_textarea_set_one_line(ui->screen_main_txb_ki, false);
	#if LV_USE_KEYBOARD != 0 || LV_USE_ZH_KEYBOARD != 0
		lv_obj_add_event_cb(ui->screen_main_txb_ki, ta_event_cb, LV_EVENT_ALL, ui->g_kb_screen_main);
	#endif
	lv_obj_set_pos(ui->screen_main_txb_ki, 142, 6);
	lv_obj_set_size(ui->screen_main_txb_ki, 200, 35);

	//Write style for screen_main_txb_ki, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_txb_ki, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_txb_ki, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_txb_ki, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_txb_ki, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_txb_ki, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_ki, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_txb_ki, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_txb_ki, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_txb_ki, lv_color_hex(0xe6e6e6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_txb_ki, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_txb_ki, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_txb_ki, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_txb_ki, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_txb_ki, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_ki, 4, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_txb_ki, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_txb_ki, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_ki, lv_color_hex(0x2195f6), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_ki, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write codes screen_main_cont_12
	ui->screen_main_cont_12 = lv_obj_create(ui->screen_main_tabview_1_tab_3);
	lv_obj_set_pos(ui->screen_main_cont_12, 16, 195);
	lv_obj_set_size(ui->screen_main_cont_12, 345, 47);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_12, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_12, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_12, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_13
	ui->screen_main_label_13 = lv_label_create(ui->screen_main_cont_12);
	lv_label_set_text(ui->screen_main_label_13, "Kd");
	lv_label_set_long_mode(ui->screen_main_label_13, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_13, 27, 15);
	lv_obj_set_size(ui->screen_main_label_13, 90, 17);

	//Write style for screen_main_label_13, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_13, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_13, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_13, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_13, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_txb_kd
	ui->screen_main_txb_kd = lv_textarea_create(ui->screen_main_cont_12);
	lv_textarea_set_text(ui->screen_main_txb_kd, "0");
	lv_textarea_set_password_bullet(ui->screen_main_txb_kd, "*");
	lv_textarea_set_password_mode(ui->screen_main_txb_kd, false);
	lv_textarea_set_one_line(ui->screen_main_txb_kd, false);
	#if LV_USE_KEYBOARD != 0 || LV_USE_ZH_KEYBOARD != 0
		lv_obj_add_event_cb(ui->screen_main_txb_kd, ta_event_cb, LV_EVENT_ALL, ui->g_kb_screen_main);
	#endif
	lv_obj_set_pos(ui->screen_main_txb_kd, 142, 6);
	lv_obj_set_size(ui->screen_main_txb_kd, 200, 35);

	//Write style for screen_main_txb_kd, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_txb_kd, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_txb_kd, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_txb_kd, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_txb_kd, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_txb_kd, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_kd, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_txb_kd, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_txb_kd, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_txb_kd, lv_color_hex(0xe6e6e6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_txb_kd, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_txb_kd, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_txb_kd, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_txb_kd, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_txb_kd, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_kd, 4, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_txb_kd, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_txb_kd, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_kd, lv_color_hex(0x2195f6), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_kd, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write codes 输出
	ui->screen_main_tabview_1_tab_4 = lv_tabview_add_tab(ui->screen_main_tabview_1,"输出");
	lv_obj_t * screen_main_tabview_1_tab_4_label = lv_label_create(ui->screen_main_tabview_1_tab_4);
	lv_label_set_text(screen_main_tabview_1_tab_4_label, "");

	//Write codes screen_main_cont_13
	ui->screen_main_cont_13 = lv_obj_create(ui->screen_main_tabview_1_tab_4);
	lv_obj_set_pos(ui->screen_main_cont_13, 16, 17);
	lv_obj_set_size(ui->screen_main_cont_13, 346, 47);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_13, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_13, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_13, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_14
	ui->screen_main_label_14 = lv_label_create(ui->screen_main_cont_13);
	lv_label_set_text(ui->screen_main_label_14, "OutTime");
	lv_label_set_long_mode(ui->screen_main_label_14, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_14, 27, 15);
	lv_obj_set_size(ui->screen_main_label_14, 90, 17);

	//Write style for screen_main_label_14, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_14, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_14, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_14, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_14, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_txb_out_time
	ui->screen_main_txb_out_time = lv_textarea_create(ui->screen_main_cont_13);
	lv_textarea_set_text(ui->screen_main_txb_out_time, "0");
	lv_textarea_set_password_bullet(ui->screen_main_txb_out_time, "*");
	lv_textarea_set_password_mode(ui->screen_main_txb_out_time, false);
	lv_textarea_set_one_line(ui->screen_main_txb_out_time, false);
	#if LV_USE_KEYBOARD != 0 || LV_USE_ZH_KEYBOARD != 0
		lv_obj_add_event_cb(ui->screen_main_txb_out_time, ta_event_cb, LV_EVENT_ALL, ui->g_kb_screen_main);
	#endif
	lv_obj_set_pos(ui->screen_main_txb_out_time, 143, 6);
	lv_obj_set_size(ui->screen_main_txb_out_time, 200, 35);

	//Write style for screen_main_txb_out_time, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_txb_out_time, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_txb_out_time, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_txb_out_time, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_txb_out_time, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_txb_out_time, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_out_time, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_txb_out_time, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_txb_out_time, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_txb_out_time, lv_color_hex(0xe6e6e6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_txb_out_time, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_txb_out_time, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_txb_out_time, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_txb_out_time, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_txb_out_time, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_out_time, 4, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_txb_out_time, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_txb_out_time, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_out_time, lv_color_hex(0x2195f6), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_out_time, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write codes screen_main_cont_14
	ui->screen_main_cont_14 = lv_obj_create(ui->screen_main_tabview_1_tab_4);
	lv_obj_set_pos(ui->screen_main_cont_14, 19, 76);
	lv_obj_set_size(ui->screen_main_cont_14, 344, 47);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_14, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_14, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_14, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_15
	ui->screen_main_label_15 = lv_label_create(ui->screen_main_cont_14);
	lv_label_set_text(ui->screen_main_label_15, "OutMax");
	lv_label_set_long_mode(ui->screen_main_label_15, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_15, 27, 15);
	lv_obj_set_size(ui->screen_main_label_15, 90, 17);

	//Write style for screen_main_label_15, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_15, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_15, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_15, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_15, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_txb_out_max
	ui->screen_main_txb_out_max = lv_textarea_create(ui->screen_main_cont_14);
	lv_textarea_set_text(ui->screen_main_txb_out_max, "0");
	lv_textarea_set_password_bullet(ui->screen_main_txb_out_max, "*");
	lv_textarea_set_password_mode(ui->screen_main_txb_out_max, false);
	lv_textarea_set_one_line(ui->screen_main_txb_out_max, false);
	#if LV_USE_KEYBOARD != 0 || LV_USE_ZH_KEYBOARD != 0
		lv_obj_add_event_cb(ui->screen_main_txb_out_max, ta_event_cb, LV_EVENT_ALL, ui->g_kb_screen_main);
	#endif
	lv_obj_set_pos(ui->screen_main_txb_out_max, 143, 6);
	lv_obj_set_size(ui->screen_main_txb_out_max, 200, 35);

	//Write style for screen_main_txb_out_max, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_txb_out_max, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_txb_out_max, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_txb_out_max, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_txb_out_max, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_txb_out_max, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_out_max, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_txb_out_max, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_txb_out_max, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_txb_out_max, lv_color_hex(0xe6e6e6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_txb_out_max, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_txb_out_max, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_txb_out_max, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_txb_out_max, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_txb_out_max, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_out_max, 4, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_txb_out_max, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_txb_out_max, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_out_max, lv_color_hex(0x2195f6), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_out_max, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write codes screen_main_cont_15
	ui->screen_main_cont_15 = lv_obj_create(ui->screen_main_tabview_1_tab_4);
	lv_obj_set_pos(ui->screen_main_cont_15, 19, 138);
	lv_obj_set_size(ui->screen_main_cont_15, 344, 47);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_15, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_15, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_15, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_16
	ui->screen_main_label_16 = lv_label_create(ui->screen_main_cont_15);
	lv_label_set_text(ui->screen_main_label_16, "OutMin");
	lv_label_set_long_mode(ui->screen_main_label_16, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_16, 27, 15);
	lv_obj_set_size(ui->screen_main_label_16, 90, 17);

	//Write style for screen_main_label_16, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_16, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_16, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_16, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_16, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_txb_out_min
	ui->screen_main_txb_out_min = lv_textarea_create(ui->screen_main_cont_15);
	lv_textarea_set_text(ui->screen_main_txb_out_min, "0");
	lv_textarea_set_password_bullet(ui->screen_main_txb_out_min, "*");
	lv_textarea_set_password_mode(ui->screen_main_txb_out_min, false);
	lv_textarea_set_one_line(ui->screen_main_txb_out_min, false);
	#if LV_USE_KEYBOARD != 0 || LV_USE_ZH_KEYBOARD != 0
		lv_obj_add_event_cb(ui->screen_main_txb_out_min, ta_event_cb, LV_EVENT_ALL, ui->g_kb_screen_main);
	#endif
	lv_obj_set_pos(ui->screen_main_txb_out_min, 143, 6);
	lv_obj_set_size(ui->screen_main_txb_out_min, 200, 35);

	//Write style for screen_main_txb_out_min, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_txb_out_min, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_txb_out_min, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_txb_out_min, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_txb_out_min, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_txb_out_min, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_out_min, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_txb_out_min, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_txb_out_min, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_txb_out_min, lv_color_hex(0xe6e6e6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_txb_out_min, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_txb_out_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_txb_out_min, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_txb_out_min, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_txb_out_min, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_out_min, 4, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_txb_out_min, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_txb_out_min, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_out_min, lv_color_hex(0x2195f6), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_out_min, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write codes screen_main_cont_16
	ui->screen_main_cont_16 = lv_obj_create(ui->screen_main_tabview_1_tab_4);
	lv_obj_set_pos(ui->screen_main_cont_16, 20, 191);
	lv_obj_set_size(ui->screen_main_cont_16, 343, 47);
	lv_obj_set_scrollbar_mode(ui->screen_main_cont_16, LV_SCROLLBAR_MODE_OFF);

	//Write style for screen_main_cont_16, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_cont_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_cont_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_cont_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_cont_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_cont_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_cont_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_cont_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_cont_16, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_label_17
	ui->screen_main_label_17 = lv_label_create(ui->screen_main_cont_16);
	lv_label_set_text(ui->screen_main_label_17, "Out");
	lv_label_set_long_mode(ui->screen_main_label_17, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->screen_main_label_17, 27, 15);
	lv_obj_set_size(ui->screen_main_label_17, 90, 17);

	//Write style for screen_main_label_17, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_label_17, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_label_17, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_label_17, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_label_17, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_label_17, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_txb_out
	ui->screen_main_txb_out = lv_textarea_create(ui->screen_main_cont_16);
	lv_textarea_set_text(ui->screen_main_txb_out, "0");
	lv_textarea_set_password_bullet(ui->screen_main_txb_out, "*");
	lv_textarea_set_password_mode(ui->screen_main_txb_out, false);
	lv_textarea_set_one_line(ui->screen_main_txb_out, false);
	#if LV_USE_KEYBOARD != 0 || LV_USE_ZH_KEYBOARD != 0
		lv_obj_add_event_cb(ui->screen_main_txb_out, ta_event_cb, LV_EVENT_ALL, ui->g_kb_screen_main);
	#endif
	lv_obj_set_pos(ui->screen_main_txb_out, 143, 6);
	lv_obj_set_size(ui->screen_main_txb_out, 200, 35);

	//Write style for screen_main_txb_out, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_text_color(ui->screen_main_txb_out, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_txb_out, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->screen_main_txb_out, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_txb_out, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_txb_out, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_out, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_txb_out, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_main_txb_out, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_main_txb_out, lv_color_hex(0xe6e6e6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->screen_main_txb_out, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_txb_out, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_main_txb_out, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_txb_out, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_txb_out, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_out, 4, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_txb_out, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_txb_out, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_txb_out, lv_color_hex(0x2195f6), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_txb_out, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write codes screen_main_btn_save
	ui->screen_main_btn_save = lv_btn_create(ui->screen_main_tabview_1_tab_4);
	ui->screen_main_btn_save_label = lv_label_create(ui->screen_main_btn_save);
	lv_label_set_text(ui->screen_main_btn_save_label, "Save");
	lv_label_set_long_mode(ui->screen_main_btn_save_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->screen_main_btn_save_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->screen_main_btn_save, 0, LV_STATE_DEFAULT);
	lv_obj_set_pos(ui->screen_main_btn_save, 174, 249);
	lv_obj_set_size(ui->screen_main_btn_save, 107, 28);

	//Write style for screen_main_btn_save, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_btn_save, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_btn_save, lv_color_hex(0x2195f6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_btn_save, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_btn_save, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_btn_save, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_btn_save, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_btn_save, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_btn_save, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes 自整定
	ui->screen_main_tabview_1_tab_5 = lv_tabview_add_tab(ui->screen_main_tabview_1,"自整定");
	lv_obj_t * screen_main_tabview_1_tab_5_label = lv_label_create(ui->screen_main_tabview_1_tab_5);
	lv_label_set_text(screen_main_tabview_1_tab_5_label, "");

	//Write codes screen_main_btn_4
	ui->screen_main_btn_4 = lv_btn_create(ui->screen_main_tabview_1_tab_5);
	ui->screen_main_btn_4_label = lv_label_create(ui->screen_main_btn_4);
	lv_label_set_text(ui->screen_main_btn_4_label, "开始");
	lv_label_set_long_mode(ui->screen_main_btn_4_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->screen_main_btn_4_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->screen_main_btn_4, 0, LV_STATE_DEFAULT);
	lv_obj_set_pos(ui->screen_main_btn_4, 216, 243);
	lv_obj_set_size(ui->screen_main_btn_4, 100, 35);

	//Write style for screen_main_btn_4, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_btn_4, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_btn_4, lv_color_hex(0x2195f6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_btn_4, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_btn_4, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_btn_4, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_btn_4, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_btn_4, &lv_font_simhei_25, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_btn_4, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_btn_3
	ui->screen_main_btn_3 = lv_btn_create(ui->screen_main_tabview_1_tab_5);
	ui->screen_main_btn_3_label = lv_label_create(ui->screen_main_btn_3);
	lv_label_set_text(ui->screen_main_btn_3_label, "结束");
	lv_label_set_long_mode(ui->screen_main_btn_3_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->screen_main_btn_3_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->screen_main_btn_3, 0, LV_STATE_DEFAULT);
	lv_obj_set_pos(ui->screen_main_btn_3, 76, 243);
	lv_obj_set_size(ui->screen_main_btn_3, 100, 35);

	//Write style for screen_main_btn_3, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->screen_main_btn_3, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_main_btn_3, lv_color_hex(0xf00000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_main_btn_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->screen_main_btn_3, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_btn_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_main_btn_3, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_main_btn_3, &lv_font_simhei_25, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->screen_main_btn_3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_main_spinner_2
	ui->screen_main_spinner_2 = lv_spinner_create(ui->screen_main_tabview_1_tab_5, 1000, 60);
	lv_obj_set_pos(ui->screen_main_spinner_2, 91, 16);
	lv_obj_set_size(ui->screen_main_spinner_2, 203, 188);

	//Write style for screen_main_spinner_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_pad_top(ui->screen_main_spinner_2, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_main_spinner_2, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_main_spinner_2, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_main_spinner_2, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_main_spinner_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_arc_width(ui->screen_main_spinner_2, 12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_arc_opa(ui->screen_main_spinner_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_arc_color(ui->screen_main_spinner_2, lv_color_hex(0xd5d6de), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_main_spinner_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for screen_main_spinner_2, Part: LV_PART_INDICATOR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_arc_width(ui->screen_main_spinner_2, 12, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_arc_opa(ui->screen_main_spinner_2, 255, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_arc_color(ui->screen_main_spinner_2, lv_color_hex(0x00ff71), LV_PART_INDICATOR|LV_STATE_DEFAULT);

	//Update current screen layout.
	lv_obj_update_layout(ui->screen_main);

	
	//Init events for screen.
	events_init_screen_main(ui);
}
