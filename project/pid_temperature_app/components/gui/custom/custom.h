// SPDX-License-Identifier: MIT
// Copyright 2020 NXP

/*
 * custom.h
 *
 *  Created on: July 29, 2020
 *      Author: nxf53801
 */

#ifndef __CUSTOM_H_
#define __CUSTOM_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "gui_guider.h"

extern void ui_screen_main_event_init(void);
void __attribute__((weak)) ui_screen_main_event_init(void) {

}
extern void ui_screen_sample_id_event_init(void);
void __attribute__((weak)) ui_screen_sample_id_event_init(void) {

}
extern void ui_screen_menu_list_event_init(void);
void __attribute__((weak)) ui_screen_menu_list_event_init(void) {

}
extern void ui_screen_wifi_event_init(void);
void __attribute__((weak)) ui_screen_wifi_event_init(void) {

}
extern void ui_screen_data_event_init(void);
void __attribute__((weak)) ui_screen_data_event_init(void) {

}
extern void ui_screen_ble_event_init(void);
void __attribute__((weak)) ui_screen_ble_event_init(void) {

}
extern void ui_screen_calib_event_init(void);
void __attribute__((weak)) ui_screen_calib_event_init(void) {
}
void custom_init(lv_ui *ui);

#ifdef __cplusplus
}
#endif
#endif /* EVENT_CB_H_ */
