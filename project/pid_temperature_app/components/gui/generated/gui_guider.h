/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#ifndef GUI_GUIDER_H
#define GUI_GUIDER_H
#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"

typedef struct
{
  
	lv_obj_t *screen_main;
	bool screen_main_del;
	lv_obj_t *g_kb_screen_main;
	lv_obj_t *screen_main_tabview_1;
	lv_obj_t *screen_main_tabview_1_tab_1;
	lv_obj_t *screen_main_tabview_1_tab_2;
	lv_obj_t *screen_main_tabview_1_tab_3;
	lv_obj_t *screen_main_tabview_1_tab_4;
	lv_obj_t *screen_main_tabview_1_tab_5;
	lv_obj_t *screen_main_cont_1;
	lv_obj_t *screen_main_label_1;
	lv_obj_t *screen_main_label_pv_value;
	lv_obj_t *screen_main_cont_2;
	lv_obj_t *screen_main_label_3;
	lv_obj_t *screen_main_label_sv_value;
	lv_obj_t *screen_main_led_out;
	lv_obj_t *screen_main_chart_temperature;
	lv_obj_t *screen_main_cont_9;
	lv_obj_t *screen_main_label_10;
	lv_obj_t *screen_main_txb_sv;
	lv_obj_t *screen_main_cont_10;
	lv_obj_t *screen_main_label_11;
	lv_obj_t *screen_main_txb_kp;
	lv_obj_t *screen_main_cont_11;
	lv_obj_t *screen_main_label_12;
	lv_obj_t *screen_main_txb_ki;
	lv_obj_t *screen_main_cont_12;
	lv_obj_t *screen_main_label_13;
	lv_obj_t *screen_main_txb_kd;
	lv_obj_t *screen_main_cont_13;
	lv_obj_t *screen_main_label_14;
	lv_obj_t *screen_main_txb_out_time;
	lv_obj_t *screen_main_cont_14;
	lv_obj_t *screen_main_label_15;
	lv_obj_t *screen_main_txb_out_max;
	lv_obj_t *screen_main_cont_15;
	lv_obj_t *screen_main_label_16;
	lv_obj_t *screen_main_txb_out_min;
	lv_obj_t *screen_main_cont_16;
	lv_obj_t *screen_main_label_17;
	lv_obj_t *screen_main_txb_out;
	lv_obj_t *screen_main_btn_save;
	lv_obj_t *screen_main_btn_save_label;
	lv_obj_t *screen_main_btn_4;
	lv_obj_t *screen_main_btn_4_label;
	lv_obj_t *screen_main_btn_3;
	lv_obj_t *screen_main_btn_3_label;
	lv_obj_t *screen_main_spinner_2;
}lv_ui;

void ui_init_style(lv_style_t * style);
void init_scr_del_flag(lv_ui *ui);
void setup_ui(lv_ui *ui);
extern lv_ui guider_ui;

void setup_scr_screen_main(lv_ui *ui);

LV_FONT_DECLARE(lv_font_simhei_30)
LV_FONT_DECLARE(lv_font_simhei_20)
LV_FONT_DECLARE(lv_font_simsun_12)
LV_FONT_DECLARE(lv_font_simsun_16)
LV_FONT_DECLARE(lv_font_montserratMedium_35)
LV_FONT_DECLARE(lv_font_montserratMedium_16)
LV_FONT_DECLARE(lv_font_montserratMedium_12)
LV_FONT_DECLARE(lv_font_simhei_25)
LV_FONT_DECLARE(lv_font_simhei_18)


#ifdef __cplusplus
}
#endif
#endif
