// Queue ADT implementation ... COMP2521

#include "queue.h"
#include <assert.h>
#include <stdlib.h>

// set up empty queue
queue newQueue()
{
    queue Q = malloc(sizeof(QueueRep));
    Q->length = 0;
    Q->head = NULL;
    Q->tail = NULL;
    return Q;
}

// remove unwanted queue
void dropQueue(queue Q)
{
    NodeT *curr = Q->head;
    while (curr != NULL)
    {
        NodeT *temp = curr->next;
        free(curr);
        curr = temp;
    }
    free(Q);
}

// check whether queue is empty
int QueueIsEmpty(queue Q) { return (Q->length == 0); }

// insert an int at end of queue
void QueueEnqueue(queue Q, int v)
{
    NodeT *new = malloc(sizeof(NodeT));
    if (new == NULL)
    {
        return;
    }
    new->data = v;
    new->next = NULL;
    if (Q->tail != NULL)
    {
        Q->tail->next = new;
        Q->tail = new;
    }
    else
    {
        Q->head = new;
        Q->tail = new;
    }
    Q->length++;
}

// remove int from front of queue
int QueueDequeue(queue Q)
{
    NodeT *p = Q->head;

    if (p == NULL)
    {
        return;
    }
    Q->head = Q->head->next;
    if (Q->head == NULL)
    {
        Q->tail = NULL;
    }
    Q->length--;
    int d = p->data;
    free(p);
    return d;
}
void QueueGetValue(queue Q, int *data, int *size)
{
    if (Q->length <= 0)
    {
        *size = 0;
        return;
    }
    NodeT *p = Q->head;

    for (int i = 0; i < Q->length; i++)
    {

        data[i] = p->data;

        *size = i + 1;
        if (p->next == NULL)
        {
            break;
        }
        p = p->next;
    }
}
