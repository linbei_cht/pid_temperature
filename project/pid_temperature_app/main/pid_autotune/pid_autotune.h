#ifndef _PID_AUTOTUNE_H
#define _PID_AUTOTUNE_H
#define LIBRARY_VERSION 0.0.1
#include <stdbool.h>
typedef enum
{
    TYPE_PI = 0,
    TYPE_PID
} pid_type_e;

typedef enum
{
    AUTOTUNE_FINISHED = 1,
    AUTOTUNE_UNFINISHED = 0
} pid_autotune_state;

typedef struct PID_ATune
{
    bool isMax, isMin;
    float *input, *output;
    float setpoint;
    float noiseBand;
    pid_type_e controlType;
    bool running;
    unsigned long peak1, peak2, lastTime;
    int sampleTime;
    int nLookBack;
    int peakType;
    float lastInputs[101];
    float peaks[10];
    int peakCount;
    bool justchanged;
    bool justevaled;
    float absMax, absMin;
    float oStep;
    float outputStart;
    float Ku, Pu;

} pid_autotune_t;

// commonly used functions **************************************************************************


/// @brief 创建一个pid自整定结构体
/// @param input 输入值地址
/// @param output 输出值地址
/// @return 
pid_autotune_t *pid_autotune_create(float *input, float *output); // * Constructor.  links the Autotune to a given PID
void pid_autotune_free(pid_autotune_t *handle);
pid_autotune_state pat_runtime(pid_autotune_t *handle);                // * Similar to the PID Compue function, returns non 0 when done
void pat_cancel(pid_autotune_t *handle);                // * Stops the AutoTune
void pat_finish_up(pid_autotune_t *handle);
void pat_set_output_step(pid_autotune_t *handle,float step); // * how far above and below the starting value will the output step?
float pat_get_output_step(pid_autotune_t *handle);     //

void pat_set_control_type(pid_autotune_t *handle, pid_type_e type); // * Determies if the tuning parameters returned will be PI (D=0)
pid_type_e pat_get_control_type(pid_autotune_t *handle);                   //   or PID.  (0=PI, 1=PID)

void pat_set_lookback_sec(pid_autotune_t *handle, int value); // * how far back are we looking to identify peaks
int pat_get_lookback_sec(); // * how far back are we looking to identify peaks

void pat_set_noise_band(pid_autotune_t *handle, float noise); // * the autotune will ignore signal chatter smaller than this value
float pat_get_noise_band(pid_autotune_t *handle);       //   this should be acurately set

float pat_get_kp(pid_autotune_t *handle); // * once autotune is complete, these functions contain the
float pat_get_ki(pid_autotune_t *handle); //   computed tuning parameters.
float pat_get_kd(pid_autotune_t *handle);
#endif