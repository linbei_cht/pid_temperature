#ifndef _UPDATE_VALUE_TASK_H
#define _UPDATE_VALUE_TASK_H
#include "gui_guider.h"

#ifdef __cplusplus

extern "C"
{
#endif
/// @brief 更新传感器任务
/// @param pvParameters

void init_update_pid_task(void * pvParameters);
void init_update_ui_task(void * pvParameters);
void init_runtime_read_temperature(void * pvParameters);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

