#include "time_utils.h"
#include "qmsd_utils.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "esp_system.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <math.h>
#include <time.h>
#include "qmsd_utils.h"
#include "esp_err.h"
#include "esp_sleep.h"
 void rand_calue_init()
{
    time_t t;
    /* 初始化随机数发生器 */
    srand((unsigned)time(&t));
}
 void rand_value_create(float *value)
{
    *value = rand() % 100 + rand() % 100 / 1000.0f;
}
/// @brief 设置当前时间
/// @param year
/// @param month
/// @param day
/// @param hour
/// @param min
/// @param sec
 void set_currment_time(int year, int month, int day, int hour, int min, int sec)
{
    struct tm timeptr;
    timeptr.tm_year = year - 1900;
    timeptr.tm_mon = month - 1;
    timeptr.tm_mday = day;
    timeptr.tm_hour = hour;
    timeptr.tm_min = min;
    timeptr.tm_sec = sec;
    time_t time;
    time = mktime(&timeptr);
    qmsd_time_set_time(time);
    qmsd_time_set_timezone_cst_8();
}