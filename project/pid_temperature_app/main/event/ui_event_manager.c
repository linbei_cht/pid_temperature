#include "ui_event_manager.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "esp_system.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "data_shared.h"
#include "gui_guider.h"
#include "lvgl.h"

#define TAG "ui-event-manager"
#define PV_DATA_COUNT 40
lv_ui guider_ui;
static lv_chart_series_t *temperature_series;

static uint32_t _pv_data[PV_DATA_COUNT] = {0};
static char _pv_str[30] = {0};
static char _out_str[30] = {0};
static int _point_size = 0;
static pid_cache_t pid_cache = {0};

static char _kp_s[32] = {0};
static char _ki_s[32] = {0};
static char _kd_s[32] = {0};
static char _outmax_s[32] = {0};
static char _outmin_s[32] = {0};
static char _outtime_s[32] = {0};
static char _sv_s[32] = {0};
static bool _led_state = false;
static void load_nvs_flash_data()
{

    db_get_pid_cache(&pid_cache);
    memset(_kp_s, 0, 32);
    memset(_ki_s, 0, 32);
    memset(_kd_s, 0, 32);
    memset(_outmax_s, 0, 32);
    memset(_outmin_s, 0, 32);
    memset(_outtime_s, 0, 32);
    memset(_sv_s, 0, 32);

    sprintf(_kp_s, "%.4f", pid_cache.kp);
    sprintf(_ki_s, "%.4f", pid_cache.ki);
    sprintf(_kd_s, "%.4f", pid_cache.kd);
    sprintf(_outmax_s, "%.4f", pid_cache.out_max);
    sprintf(_outmin_s, "%.4f", pid_cache.out_min);
    sprintf(_outtime_s, "%u", pid_cache.out_time_ms);
    sprintf(_sv_s, "%.3f", pid_cache.sv);

    ESP_LOGI(TAG, "%s,%f", _sv_s, pid_cache.sv);

    lv_textarea_set_text(guider_ui.screen_main_txb_kp, _kp_s);
    lv_textarea_set_text(guider_ui.screen_main_txb_ki, _ki_s);
    lv_textarea_set_text(guider_ui.screen_main_txb_kd, _kd_s);
    lv_textarea_set_text(guider_ui.screen_main_txb_out_max, _outmax_s);
    lv_textarea_set_text(guider_ui.screen_main_txb_out_min, _outmin_s);
    lv_textarea_set_text(guider_ui.screen_main_txb_out_time, _outtime_s);
    lv_textarea_set_text(guider_ui.screen_main_txb_sv, _sv_s);
    lv_label_set_text(guider_ui.screen_main_label_sv_value, _sv_s);
}
static void save_nvs_flash_data()
{

    const char *kp_s = lv_textarea_get_text(guider_ui.screen_main_txb_kp);
    const char *ki_s = lv_textarea_get_text(guider_ui.screen_main_txb_ki);
    const char *kd_s = lv_textarea_get_text(guider_ui.screen_main_txb_kd);
    const char *outmax_s = lv_textarea_get_text(guider_ui.screen_main_txb_out_max);
    const char *outmin_s = lv_textarea_get_text(guider_ui.screen_main_txb_out_min);
    const char *outtime_s = lv_textarea_get_text(guider_ui.screen_main_txb_out_time);
    const char *sv_s = lv_textarea_get_text(guider_ui.screen_main_txb_sv);

    if (strlen(kp_s) > 0)
    {
        pid_cache.kp = atof(kp_s);
    }

    if (strlen(ki_s) > 0)
    {
        pid_cache.ki = atof(ki_s);
    }

    if (strlen(kd_s) > 0)
    {
        pid_cache.kd = atof(kd_s);
    }

    if (strlen(outmax_s) > 0)
    {
        pid_cache.out_max = atof(outmax_s);
    }

    if (strlen(outmin_s) > 0)
    {
        pid_cache.out_min = atof(outmin_s);
    }

    if (strlen(outtime_s) > 0)
    {
        pid_cache.out_time_ms = (uint32_t)atoi(outtime_s);
        ESP_LOGI(TAG, "%d", atoi(outtime_s));
    }

    if (strlen(sv_s) > 0)
    {
        pid_cache.sv = atof(sv_s);
    }
    db_set_pid_cache(&pid_cache);
}
static void updata_chart_value()
{
    db_get_u32_data(_pv_data, &_point_size);

    for (int i = 0; i < PV_DATA_COUNT; i++)
    {
        lv_chart_set_next_value(guider_ui.screen_main_chart_temperature, temperature_series, _pv_data[i]);
    }

    lv_chart_refresh(guider_ui.screen_main_chart_temperature);
}
static void update_pv_out_value()
{

    memset(_pv_str, 0, 30);
    memset(_out_str, 0, 30);
    sprintf(_pv_str, "%.2f", db_get_input());
    sprintf(_out_str, "%u", db_get_output());
    lv_label_set_text(guider_ui.screen_main_label_pv_value, _pv_str);
    lv_textarea_set_text(guider_ui.screen_main_txb_out, _out_str);
}
static void all_value_update_event_handler(lv_event_t *event)
{

    update_pv_out_value();
    vTaskDelay(pdMS_TO_TICKS(100));
    updata_chart_value();
    vTaskDelay(pdMS_TO_TICKS(100));
}
static void led_update_event_handler(lv_event_t *e)
{
    if (_led_state)
    {
        lv_led_on(guider_ui.screen_main_led_out);
    }
    else
    {
        lv_led_off(guider_ui.screen_main_led_out);
    }
    //  lv_led_toggle(guider_ui.screen_main_led_out);
}
static void btn_set_autotune_enable_model(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);

    if (code == LV_EVENT_CLICKED || LV_EVENT_VALUE_CHANGED)
    {
        db_set_autotune_state(true);
        lv_obj_set_style_arc_color(guider_ui.screen_main_spinner_2, lv_color_hex(0xf00000), LV_PART_INDICATOR|LV_STATE_DEFAULT);
    }
  
   
}
static void btn_set_autotune_disenable_model(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);

    if (code == LV_EVENT_CLICKED ||LV_EVENT_VALUE_CHANGED )
    {
         db_set_autotune_state(false);
        lv_obj_set_style_arc_color(guider_ui.screen_main_spinner_2, lv_color_hex(0x00ff71), LV_PART_INDICATOR|LV_STATE_DEFAULT);
    }
   
}
	
static void btn_save_event_handler(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);

    if (code == LV_EVENT_CLICKED)
    {
        save_nvs_flash_data();
    }
}
void ui_update_led(bool state)
{
    _led_state = state;
    lv_event_send(guider_ui.screen_main_led_out, LV_EVENT_VALUE_CHANGED, NULL);
}
void ui_update_all_data()
{
    lv_event_send(guider_ui.screen_main_label_pv_value, LV_EVENT_VALUE_CHANGED, NULL);
}

void ui_screen_main_event_init()
{
    lv_keyboard_set_mode(guider_ui.g_kb_screen_main, LV_KEYBOARD_MODE_NUMBER);
    temperature_series = lv_chart_add_series(guider_ui.screen_main_chart_temperature, lv_color_hex(0x2ac83c), LV_CHART_AXIS_PRIMARY_Y);
    lv_chart_set_point_count(guider_ui.screen_main_chart_temperature, PV_DATA_COUNT);

    lv_obj_add_event_cb(guider_ui.screen_main_label_pv_value, all_value_update_event_handler, LV_EVENT_VALUE_CHANGED, NULL);
    lv_obj_add_event_cb(guider_ui.screen_main_led_out, led_update_event_handler, LV_EVENT_VALUE_CHANGED, NULL);

    lv_obj_add_event_cb(guider_ui.screen_main_btn_save, btn_save_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(guider_ui.screen_main_btn_3, btn_set_autotune_disenable_model, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(guider_ui.screen_main_btn_4, btn_set_autotune_enable_model, LV_EVENT_ALL, NULL);

    load_nvs_flash_data();
}
void ui_set_autotune(bool state)
{
    db_set_autotune_state(state);
    lv_event_send(guider_ui.screen_main_btn_3, LV_EVENT_VALUE_CHANGED, NULL);
}