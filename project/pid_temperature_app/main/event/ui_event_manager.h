#ifndef _UI_EVENT_MANAGER_H
#define _UI_EVENT_MANAGER_H
#include "gui_guider.h"
#include <stdbool.h>
#ifdef __cplusplus

extern "C"
{
#endif
void ui_screen_main_event_init();
void ui_update_all_data();
void ui_update_led(bool state);
void ui_set_autotune(bool state);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

