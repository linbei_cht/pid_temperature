#include "gpio_pwm_out.h"
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#define TAG "gpio-out"
void gpio_out_config(int pin)
{
    ESP_LOGI(TAG, "configure gpio init!");
    gpio_reset_pin(pin);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(pin, GPIO_MODE_OUTPUT);
}
void gpio_set_state(int pin,bool state)
{
    gpio_set_level(pin, state);
}
