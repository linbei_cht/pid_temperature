#ifndef __GPIO_PWM_OUT_H
#define __GPIO_PWM_OUT_H

#include <stdbool.h>
void gpio_out_config(int pin);
void gpio_set_state(int pin,bool state);
#endif