#ifndef _TEMPERATURE_READER_H
#define _TEMPERATURE_READER_H
/// @brief 温控模块通讯初始化
void temp_ctrl_uart_init();
/// @brief 温控模块通讯释放
void temp_ctrl_uart_free();
/// @brief 获取温度
/// @param value 
void temp_ctrl_read(float* value);

#endif 



