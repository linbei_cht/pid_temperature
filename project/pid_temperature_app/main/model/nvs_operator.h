/// 用于操作nvs

#ifndef _NVS_OPERATOR_H
#define _NVS_OPERATOR_H
#include <stdint.h>

#ifdef __cplusplus

extern "C"
{
#endif
void nvs_op_init();
void nvs_op_get_float_value(const char * key,float* value,float default_value);
void nvs_op_set_float_value(const char * key,float value);

void nvs_op_get_int32_value(const char * key,int32_t*  value,int32_t default_value);
void nvs_op_set_int32_value(const char * key,int32_t value);

void nvs_op_get_u32_value(const char * key,uint32_t*  value,uint32_t default_value);
void nvs_op_set_u32_value(const char * key,uint32_t value);

void nvs_op_get_string(const char * key,char* str,int16_t size,char* default_str);
void nvs_op_set_string(const char * key,char* str,int16_t size);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif