/// 用于共享数据

#ifndef _DATA_SHARED_H
#define _DATA_SHARED_H
#include <stdint.h>
#include <stdbool.h>
#ifdef __cplusplus

extern "C"
{
#endif
    typedef struct pid_cache
    {
        float sv;
        float kp;
        float ki;
        float kd;
        float out_max;
        float out_min;
        uint32_t out_time_ms;

        /* data */
    } pid_cache_t;

    void db_init();
    void db_add_data(float value);
    void db_get_data(int *data, int *size);
    void db_get_u32_data(uint32_t *data, int *size);
    void db_clear();


    float db_get_input();
    void db_set_input(float value);

    uint32_t db_get_output();
    void db_set_output(uint32_t value);

    void db_get_pid_cache(pid_cache_t *cache);
    void db_set_pid_cache(const pid_cache_t *cache);

    void db_set_autotune_state(bool state);

    bool db_get_autotune_state();
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
