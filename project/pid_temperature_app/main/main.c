#include "data_shared.h"
#include "ui_event_manager.h"
#include "update_value_task.h"

#include <stdio.h>
#include "string.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "qmsd_board.h"
#include "qmsd_utils.h"
#include "lvgl.h"
#include "gui_guider.h"
#include "events_init.h"

#define TAG "density-main"
lv_ui guider_ui;
// 界面代码回调函数
void gui_user_init()
{

    setup_ui(&guider_ui);
    ESP_LOGI(TAG, "nvs flash init");
    db_init();
    ESP_LOGI(TAG, "events_init");
    ui_screen_main_event_init();
}
/// @brief 任务列表初始化
/*
 nvs_flask init -> gui event-> vfs_data_spi_sdcard -> file_service_init -> update_sensor

*/
void user_task_init()
{
    xTaskCreatePinnedToCore(init_update_pid_task, "pid-control-task", 4096 * 2, NULL, 0, NULL, 0);
    xTaskCreatePinnedToCore(init_update_ui_task, "update-task", 4096 * 2, NULL, 0, NULL, 1);
    xTaskCreatePinnedToCore(init_runtime_read_temperature, "read temperature", 4096 * 2, NULL, 0, NULL, 1);
}
void app_main(void)
{

    gpio_install_isr_service(ESP_INTR_FLAG_SHARED);
    qmsd_board_config_t config = QMSD_BOARD_DEFAULT_CONFIG;
    config.board_dir = BOARD_ROTATION_90;
    config.gui.refresh_task.core = 1;
    qmsd_board_init(&config);
    user_task_init();
    printf("pid temperature app!\r\n");
}
